/*Screen to register the user*/
import React from 'react';
import { View, ScrollView, KeyboardAvoidingView,StyleSheet, Text, Alert } from 'react-native';
import Mytextinput from './Mytextinput';
import Mybutton from './Mybutton';

 
import Convict from './Convict';
import Condem from './Condem';
import mental from './Mental';
import LifeInprisonments from './LifeInprisonments';
import { openDatabase } from 'react-native-sqlite-storage';
import { FAB } from 'react-native-paper';
import { FloatingAction } from "react-native-floating-action";

//var db = openDatabase({ name: 'UserDatabase.db' });
//Connction to access the pre-populated user_db.db
var db = openDatabase({ name: 'prisonsthree.db', createFromLocation : 1});
export default class Trial extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      male: '',
      female: '',
      fId: '',
    };
  }
  register_user = () => {
    var that = this;
    const { male } = this.state;
    const { female } = this.state;
    const { fId } = this.state;
    //alert(user_name, user_contact, user_address);
    if (male) {
      if (female) {
        if (fId) {
          db.transaction(function(tx) {
            tx.executeSql(
              'INSERT INTO trials (male, female, fId) VALUES (?,?,?)',
              [male, female, fId],
              (tx, results) => {
                console.log('Results', results.rowsAffected);
                if (results.rowsAffected > 0) {
                  Alert.alert(
                    'Success',
                    'No of Trials Added Successfully',
                    [
                      {
                        text: 'Ok',
                        onPress: () =>
                          that.props.navigation.navigate('HomeScreen'),
                      },
                    ],
                    { cancelable: false }
                  );
                } else {
                  alert('Registration Failed');
                }
              }
            );
          });
        } else {
          alert('Please fill Trials');
        }
      } else {
        alert('Please fill Remand');
      }
    } else {
      alert('Please fill Facility');
    }
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <KeyboardAvoidingView
            behavior="padding"
            style={{ flex: 1, justifyContent: 'space-between' }}>
            <Mytextinput
              placeholder="Enter No. OF Male"
              onChangeText={male => this.setState({ male })}
              style={{ padding:10 }}
            />
            <Mytextinput
              placeholder="Enter No. OF Female"
              onChangeText={female => this.setState({ female })}
              maxLength={10}
              keyboardType="numeric"
              style={{ padding:10 }}
            />
            <Mytextinput
              placeholder="Enter Facility ID"
              onChangeText={fId => this.setState({ fId })}
              maxLength={225}
              numberOfLines={5}
              multiline={true}
              style={{ textAlignVertical: 'top',padding:10 }}
            />
            <Mybutton
              title="Send"
              customClick={this.register_user.bind(this)}
            />

         
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TextStyle: {
    fontSize: 23,
    textAlign: 'center',
    color: '#f00',
  },
});