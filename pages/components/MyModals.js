import React, { Component } from 'react'
import { View, StyleSheet, Text, Modal, TouchableHighlight } from 'react-native'
import { bool } from 'prop-types';
import SimpleModal from './SimpleModal';
export default class MyModals extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
             isModalVisible: false,
             choosenData: '',
        };
    }
    
    changeModalVisibility = (bool) => {
        this.setState({isModalVisible: bool});
    }
   setData = (data) => {
       this.setState({choosenData: data});
   }
    render() {
        return (
            <View style={styles.contentContainer}>
               <Text style={styles.text}>
                   {this.state.choosenData}
               </Text>
                 <TouchableHighlight customClick={() => this.changeModalVisibility(true)} style={[styles.touchableHighlight, {backgroundColor: 'orange'}]}
                 underlayColor={'#f1f1f1'}>
                     <Text style={styles.text}> Open Facilities</Text>
                 </TouchableHighlight>
                 <Modal transparent={true} visible={this.state.isModalVisible} onRequestClose={()=> this.changeModalVisibility(false)}
                 animationType='fade'>
                      <SimpleModal changeModalVisibility={this.changeModalVisibility} setData={this.setData}/>
                 </Modal>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        backgroundColor: 'yellow',
        alignItems: 'center',
        justifyContent: 'center',
    },   
    
    text: {
        marginVertical: 20,
        fontSize: 20,
        fontWeight: 'bold'
    },
    touchableHighlight: {
       
        backgroundColor: 'white',
         
        alignSelf: 'stretch',
        alignItems: 'center',
           
    },
});

