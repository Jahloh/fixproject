/*Screen to update the user*/
import React from 'react';
import { View, YellowBox, ScrollView, KeyboardAvoidingView, Alert, } from 'react-native';
import Mytextinput from './components/Mytextinput';
import Mybutton from './components/Mybutton';
import { openDatabase } from 'react-native-sqlite-storage';
//var db = openDatabase({ name: 'UserDatabase.db' });

//Connction to access the pre-populated user_db.db
var db = openDatabase({ name: 'prisonsthree.db', createFromLocation : 1});
 
export default class UpdateUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input_user_id: '',
      facility: '',
      remand: '',
      trials: '',
    };
  }
  searchUser = () => {
    const {input_user_id} =this.state;
    console.log(this.state.input_user_id);
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM inmates where facility = ?',
        [input_user_id],
        (tx, results) => {
          var len = results.rows.length;
          console.log('len',len);
          if (len > 0) {
            console.log(results.rows.item(0).remand);
            this.setState({
             facility:results.rows.item(0).facility,
            });
            this.setState({
             remand:results.rows.item(0).remand,
            });
            this.setState({
             trials:results.rows.item(0).trials,
            });
          }else{
            alert('No user found');
            this.setState({
              facility:'',
              remand:'',
              trials:'',
            });
          }
        }
      );
    });
  };
  updateUser = () => {
    var that=this;
    const { input_user_id } = this.state;
    const { facility } = this.state;
    const { remand } = this.state;
    const { trials } = this.state;
    if (facility){
      if (remand){
        if (trials){
          db.transaction((tx)=> {
            tx.executeSql(
              'UPDATE inmates set facility=?, remand=? , trials=? where facility=?',
              [facility, remand, trials, input_user_id],
              (tx, results) => {
                console.log('Results',results.rowsAffected);
                if(results.rowsAffected>0){
                  Alert.alert( 'Success', 'Prisoner Transfered successfully',
                    [
                      {text: 'Ok', onPress: () => that.props.navigation.navigate('HomeScreen')},
                    ],
                    { cancelable: false }
                  );
                }else{
                  alert('Transfer Failed');
                }
              }
            );
          });
        }else{
          alert('Please fill Trials');
        }
      }else{
        alert('Please fill Remand');
      }
    }else{
      alert('Please fill Facility');
    }
  };
 
  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <KeyboardAvoidingView
            behavior="padding"
            style={{ flex: 1, justifyContent: 'space-between' }}>
            <Mytextinput
              placeholder="Enter Facility Name here"
              style={{ padding:10 }}
              onChangeText={input_user_id => this.setState({ input_user_id })}
            />
            <Mybutton
              title="Search Facility"
              customClick={this.searchUser.bind(this)}
            />
            <Mytextinput
              placeholder="Facility"
              value={this.state.facility}
              style={{ padding:10 }}
              onChangeText={facility => this.setState({ facility })}
            />
            <Mytextinput
              placeholder="Remand"
              value={''+ this.state.remand}
              onChangeText={remand => this.setState({ remand })}
              maxLength={10}
              style={{ padding:10 }}
              keyboardType="numeric"
            />
            <Mytextinput
              placeholder="Trials"
              value={''+ this.state.trials}
              onChangeText={trials => this.setState({ trials })}
              maxLength={10}
              style={{ padding:10 }}
              keyboardType="numeric"
            />
           {/* <Mytextinput
              value={this.state.trials}
              placeholder="Enter Address"
              onChangeText={trials => this.setState({ trials })}
              maxLength={10}
              style={{ padding:10 }}
              keyboardType="numeric"
            //   maxLength={225}
            //   numberOfLines={5}
            //   multiline={true}
            //   style={{textAlignVertical : 'top', padding:10}}
            /> */}
            <Mybutton
              title="Transfer Now"
              customClick={this.updateUser.bind(this)}
            />
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}