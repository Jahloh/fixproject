import * as React from 'react';
import { ScrollView,View, StyleSheet, Text, Alert, FlatList} from 'react-native';
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
import SearchableDropdown from 'react-native-searchable-dropdown';

import SelectPage from './SelectPage';

import Mytextinput from './components/Mytextinput';
import { FAB } from 'react-native-paper';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

 
import SelectPagesecond from './SelectPagesecond';

import RegisterUser from './RegisterUser';
import MyModals from './components/MyModals';
 

import { FloatingAction } from "react-native-floating-action";
import { SearchBar } from 'react-native-elements';
import { openDatabase } from 'react-native-sqlite-storage';
//var db = openDatabase({ name: 'UserDatabase.db' });
//Connction to access the pre-populated user_db.db
var db = openDatabase({ name: 'prisonsthree.db', createFromLocation : 1});

 

export default class HomeScreen extends React.Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      FlatListItems: [],
      search: '',
    };
  }

   

  updateSearch = search => {
    this.setState({ search });
  };
    componentDidMount(){
    db.transaction(tx => {
      tx.executeSql('SELECT   *  FROM SENITOTALS', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        this.setState({
          FlatListItems: temp,
        });
      });
    });
    }
 
  ListViewItemSeparator = () => {
    return (
      <View style={{ height: 2.2, width: '100%', backgroundColor: '#808080',  }} />
    );
  };
 
  render() {
    const {navigate} = this.props.navigation;
    const { search } = this.state;
    return (
       <View
         style={{
           flex: 1,
           backgroundColor: '#004D40',
           flexDirection: 'column',
          
         }}>
      
        {/* <Mytext  text="Fix Prisons Counter App" /> */}

         
          <View>
          
          
         <FlatList
           data={this.state.FlatListItems}
         ItemSeparatorComponent={this.ListViewItemSeparator}
           keyExtractor={(item, index) => index.toString()}
           renderItem={({ item }) => (
             <View key={item.fId} style={{ backgroundColor: 'white', padding: 20 }}>
               <Text>Id: {item.fId} </Text>
               <Text>Facility: {item.fName}</Text>
               <Text>Capacity         : {item.Capacity}</Text>
               <Text>No. Male Remand   : {item.TotRMale}</Text>
               <Text>No. Female Remand: {item.TotRFem}</Text>
               <Text>No. Male Trial            : {item.TotTMale}</Text>
               <Text>No. Female Trial        : {item.TotTFem}</Text>
              <Text>No. Male Convict       : {item.TotCVMale}</Text>
              <Text>No Female Convict    : {item.TotCVFem}</Text>
              <Text>No. Male Condemn    : {item.TotConMale}</Text>
              <Text>No.Female Condemn: {item.TotConFem}</Text>
              <Text>No. Life Male              : {item.TotLMale}</Text>
              <Text>No. Life Female          : {item.TotLFem}</Text>
              <Text>No. Mental Male         : {item.TotMMale}</Text>
              <Text>No. Mantal Female    : {item.TotMFem}</Text>
              <Text>Total Inmates             : {item.Total_Inmates}</Text>
              
               
               {/* <Text>TotalTrials: {item.Ttrials}</Text>
               <Text>TotalRemand: {item.Tremand}</Text> */}
             </View>
           )}
         />
       </View>
       
      
       <FAB
    style={styles.fab}
    small
     icon="camera"
     title = "Add"
     onPress={() =>
             navigate('Dash', {
               JSON_ListView_Clicked_Item: this.state.input_user_id,
             })
          }
    
   />
     
 
      </View>
  // <View>
  //   <MyModals/>
  // </View>
    );
  }
  
}
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    
  }
   
});