/*Screen to view single user*/
import React from 'react';
import { Text, View, Button } from 'react-native';
import Mytextinput from './components/Mytextinput';
import Mybutton from './components/Mybutton';
import RegisterUser from './RegisterUser';
import { openDatabase } from 'react-native-sqlite-storage';
//var db = openDatabase({ name: 'UserDatabase.db' }); 
//Connction to access the pre-populated user_db.db
var db = openDatabase({ name: 'prisonsthree.db', createFromLocation : 1});
export default class ViewUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input_user_id: '',
      userData: '',
      AseniTry: '',
      SeniData: '',
    };
  }
  searchUser = () => {
    const { input_user_id } = this.state;
    console.log(this.state.input_user_id);
    db.transaction(tx => {
      tx.executeSql(
        'SELECT fId,fName,TotRMale,TotRFem FROM SENITOTALS where fId = ?',
        [input_user_id],
        (tx, results) => {
          var len = results.rows.length;
          console.log('len', len);
          if (len > 0) {
            this.setState({
              userData: results.rows.item(0),
            });
          } else {
            alert('Wrong Input');
            this.setState({
              userData: '',
            });
          }
        }
      );
    });
  };
   
  render() {
    return (
      <View>
        <Mytextinput
          placeholder="Enter Facility"
          onChangeText={input_user_id => this.setState({ input_user_id })}
          style={{ padding:10 }}
        />
        <Mybutton
          title="Search By Facility"
          customClick={this.searchUser.bind(this)}
        />
        
        <View style={{ marginLeft: 35, marginRight: 35, marginTop: 10, }}>
          <Text>Facility ID: {this.state.userData.fId}</Text>
          <Text>facility: {this.state.userData.fName}</Text>
          <Text>Remand Male: {this.state.userData.TotRMale}</Text>
          <Text>Remand Female: {this.state.userData.TotRFem}</Text>
        </View>
        {/* <Mybutton
          title="View Total Remand"
          customClick={this.MeTotalRem.bind(this)}
        /> */}
        {/* <View style={{ marginLeft: 35, marginRight: 35, marginTop: 10 }}>
           
           
          <Text> Total Remand: {this.state.SeniData.remand}</Text>
           
        </View> */}

      </View>
    );
  }
}