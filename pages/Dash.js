/*Home Screen With buttons to navigate to different options*/
import React from 'react';
import { View } from 'react-native';
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
import SelectPage from './SelectPage';

import Trial from './components/Trial';
import Convict from './components/Convict';
import Condem from './components/Condem';
import mental from './components/Mental';
import LifeInprisonments from './components/LifeInprisonments';

import Mytextinput from './components/Mytextinput';
import { FAB } from 'react-native-paper';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import FirstPage from './FirstPage';
import SecondPage from './SecondPage';
import SelectPagesecond from './SelectPagesecond';

import RegisterUser from './RegisterUser';
 

import { FloatingAction } from "react-native-floating-action";

import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'prisonsthree.db', createFromLocation : 1});
export default class Dash extends React.Component {
  constructor(props) {
    super(props);
    db.transaction(function(txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
        [],
        function(tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_user', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
              []
            );
          }
        }
      );
    });
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#004D40',
          flexDirection: 'column',
          
        }}>
        <Mytext text="Fix Prisons Counter App" />
        <Mybutton
          title="Add Remand"
          customClick={() => this.props.navigation.navigate('Register')}
        />
        <Mybutton
          title="Add Trials"
          customClick={() => this.props.navigation.navigate('Trialss')}
        />
        <Mybutton
          title="Add Convicts"
          customClick={() => this.props.navigation.navigate('Convictss')}
        />
        <Mybutton
          title="Add Condemns"
          customClick={() => this.props.navigation.navigate('Condemns')}
        /><Mybutton
          title="Add Life Inprisonments"
          customClick={() => this.props.navigation.navigate('ForLife')}
        />
        <Mybutton
          title="Add Mental"
          customClick={() => this.props.navigation.navigate('ForMentalCases')}
        />
        
        {/* <Mybutton
          title="Transfer Inmates"
          customClick={() => this.props.navigation.navigate('Update')}
        /> */}
        <Mybutton
          title="View"
          customClick={() => this.props.navigation.navigate('View')}
        />
        {/* <Mybutton
          title="View All"
          customClick={() => this.props.navigation.navigate('ViewAll')}
        /> */}
        <Mybutton
          title="Release Inmate"
          customClick={() => this.props.navigation.navigate('Delete')}
        />
      </View>
    );
  }
}