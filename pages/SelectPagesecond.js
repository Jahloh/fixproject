/*Screen to update the user*/
import React from 'react';
import { View, YellowBox, ScrollView, KeyboardAvoidingView, StyleSheet, Text, Alert, } from 'react-native';
import Mytextinput from './components/Mytextinput';
import Mybutton from './components/Mybutton';
import { openDatabase } from 'react-native-sqlite-storage';

import { FAB } from 'react-native-paper';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import FirstPage from './FirstPage';
import SecondPage from './SecondPage';
//import SelectPagesecond from './SelectPagesecond';

import RegisterUser from './RegisterUser';
 

import { FloatingAction } from "react-native-floating-action";

//var db = openDatabase({ name: 'UserDatabase.db' });

//Connction to access the pre-populated user_db.db
var db = openDatabase({ name: 'prisonsthree.db', createFromLocation : 1});
 
export default class SelectPagesecond extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input_user_id: '',
      facility: '',
      remand: '',
      trials: '',
    };
  }
  searchUser = () => {
    const {input_user_id} =this.state;
    console.log(this.state.input_user_id);
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM inmates where user_id = ?',
        [input_user_id],
        (tx, results) => {
          var len = results.rows.length;
          console.log('len',len);
          if (len > 0) {
            console.log(results.rows.item(0).remand);
            this.setState({
             facility:results.rows.item(0).facility,
            });
            this.setState({
             remand:results.rows.item(0).remand,
            });
            this.setState({
             trials:results.rows.item(0).trials,
            });
          }else{
            alert('No user found');
            this.setState({
              facility:'',
              remand:'',
              trials:'',
            });
          }
        }
      );
    });
  };
  updateUser = () => {
    var that=this;
    const { input_user_id } = this.state;
    const { facility } = this.state;
    const { remand } = this.state;
    const { trials } = this.state;
    if (facility){
      if (remand){
        if (trials){
          db.transaction((tx)=> {
            tx.executeSql(
              'UPDATE inmates set facility=?, remand=? , trials=? where user_id=?',
              [facility, remand, trials, input_user_id],
              (tx, results) => {
                console.log('Results',results.rowsAffected);
                if(results.rowsAffected>0){
                  Alert.alert( 'Success', 'Prisoner Transfered successfully',
                    [
                      {text: 'Ok', onPress: () => that.props.navigation.navigate('HomeScreen')},
                    ],
                    { cancelable: false }
                  );
                }else{
                  alert('Transfer Failed');
                }
              }
            );
          });
        }else{
          alert('Please fill Trials');
        }
      }else{
        alert('Please fill Remand');
      }
    }else{
      alert('Please fill Facility');
    }
  };
// Ya Na for Add
  register_user = () => {
    var that = this;
    const { facility } = this.state;
    const { remand } = this.state;
    const { trials } = this.state;
    //alert(user_name, user_contact, user_address);
    if (facility) {
      if (remand) {
        if (trials) {
          db.transaction(function(tx) {
            tx.executeSql(
              'INSERT INTO inmates (facility, remand, trials) VALUES (?,?,?)',
              [facility, remand, trials],
              (tx, results) => {
                console.log('Results', results.rowsAffected);
                if (results.rowsAffected > 0) {
                  Alert.alert(
                    'Success',
                    'You Added Successfully',
                    [
                      {
                        text: 'Ok',
                        onPress: () =>
                          that.props.navigation.navigate('HomeScreen'),
                      },
                    ],
                    { cancelable: false }
                  );
                } else {
                  alert('Registration Failed');
                }
              }
            );
          });
        } else {
          alert('Please fill Trials');
        }
      } else {
        alert('Please fill Remand');
      }
    } else {
      alert('Please fill Facility');
    }
  };
 //Na Ya Add end
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <KeyboardAvoidingView
            behavior="padding"
            style={{ flex: 1, justifyContent: 'space-between' }}>
            <Mytextinput
              placeholder="Enter Facility Name here"
              style={{ padding:10 }}
              onChangeText={input_user_id => this.setState({ input_user_id })}
            />
            <Mybutton
              title="Search Facility"
              customClick={this.searchUser.bind(this)}
            />
            <Mytextinput
              placeholder="Facility"
              value={this.state.facility}
              style={{ padding:10 }}
              onChangeText={facility => this.setState({ facility })}
            />
            <Mytextinput
              placeholder="Remand"
              value={''+ this.state.remand}
              onChangeText={remand => this.setState({ remand })}
              maxLength={10}
              style={{ padding:10 }}
              keyboardType="numeric"
            />
            <Mytextinput
              placeholder="Trials"
              value={''+ this.state.trials}
              onChangeText={trials => this.setState({ trials })}
              maxLength={10}
              style={{ padding:10 }}
              keyboardType="numeric"
            />
           {/* <Mytextinput
              value={this.state.trials}
              placeholder="Enter Address"
              onChangeText={trials => this.setState({ trials })}
              maxLength={10}
              style={{ padding:10 }}
              keyboardType="numeric"
            //   maxLength={225}
            //   numberOfLines={5}
            //   multiline={true}
            //   style={{textAlignVertical : 'top', padding:10}}
            /> */}
            <Mybutton
              title="Send"
              customClick={this.register_user.bind(this)}
            />

            <Mybutton
              title="Edit"
              customClick={this.updateUser.bind(this)}
            />
            <View style={styles.container}>
        <Text>
          You are on Add Page page and the value passed from the first screen is
        </Text>
        {/*Using the navigation prop we can get the value passed from the first Screen*/}
        <Text style={styles.TextStyle}>
          {this.props.navigation.state.params.JSON_ListView_Clicked_Item}
        </Text>
        <Text style={{ marginTop: 16 }}>With Check</Text>
        {/*If you want to check the value is passed or not, 
         you can use conditional operator.*/}
        <Text style={styles.TextStyle}>
          {this.props.navigation.state.params.JSON_ListView_Clicked_Item
            ? this.props.navigation.state.params.JSON_ListView_Clicked_Item
            : 'No Value Passed'}
        </Text>
      </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
   container: {
        flex: 1,
        backgroundColor: '#fff',
        margin: 50,
        alignItems: 'center',
        justifyContent: 'center',
      },
      TextStyle: {
        fontSize: 23,
        textAlign: 'center',
        color: '#f00',
      },
    });