/*Home Screen With buttons to navigate to different options*/
//import React from 'react';
import * as React from 'react';
import { View, StyleSheet, Text, Alert } from 'react-native';
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
//import HomeScreen from './HomeScreen';
import Mytextinput from './components/Mytextinput';
import { FAB } from 'react-native-paper';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import FirstPage from './FirstPage';
import SecondPage from './SecondPage';

import RegisterUser from './RegisterUser'
 

import { FloatingAction } from "react-native-floating-action";

import { openDatabase } from 'react-native-sqlite-storage';
//var db = openDatabase({ name: 'UserDatabase.db' });
//Connction to access the pre-populated user_db.db
var db = openDatabase({ name: 'prisons.db', createFromLocation : 1});

 

export default class SelectPage extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      input_user_id: '',
      userData: '',
      AseniTry: '',
      SeniData: '',
    };
    db.transaction(function(txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='inmates'",
        [],
        function(tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS inmates', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS inmates(user_id INTEGER PRIMARY KEY AUTOINCREMENT, facility VARCHAR(20), remand INT(10), trials INT(10))',
              []
            );
          }
        }
      );
    });
  }
  searchUser = () => {
    const { input_user_id } = this.state;
    console.log(this.state.input_user_id);
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM inmates where facility = ?',
        [input_user_id],
        (tx, results) => {
          var len = results.rows.length;
          console.log('len', len);
          if (len > 0) {
            this.setState({
              userData: results.rows.item(0),
            });
          } else {
            alert('Wrong Input');
            this.setState({
              userData: '',
            });
          }
        }
      );
    });
  };

  AseniValueCondition(){
    
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#004D40',
          flexDirection: 'column',
          
        }}>
      
         <Mytext  text="Fix Prisons Counter App" />
 

       


        
      
      
       

         <Mybutton
          title="Add Inmate"
          customClick={() => this.props.navigation.navigate('Register')}
        />
        <Mybutton
          title="Transfer Inmates"
          customClick={() => this.props.navigation.navigate('Update')}
        />
        <Mybutton
          title="View"
          customClick={() => this.props.navigation.navigate('View')}
        />
        <Mybutton
          title="View All"
          customClick={() => this.props.navigation.navigate('ViewAll')}
        />
        <Mybutton
          title="Release Inmate"
          customClick={() => this.props.navigation.navigate('Delete')}
        />  

<View style={styles.container}>
        <Text>
          You are on Add Page page and the value passed from the first screen is
        </Text>
        {/*Using the navigation prop we can get the value passed from the first Screen*/}
        <Text style={styles.TextStyle}>
          {this.props.navigation.state.params.JSON_ListView_Clicked_Item}
        </Text>
        <Text style={{ marginTop: 16 }}>With Check</Text>
        {/*If you want to check the value is passed or not, 
         you can use conditional operator.*/}
        <Text style={styles.TextStyle}>
          {this.props.navigation.state.params.JSON_ListView_Clicked_Item
            ? this.props.navigation.state.params.JSON_ListView_Clicked_Item
            : 'No Value Passed'}
        </Text>
      </View>

      </View>
    );
  }
  
}
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
 container: {
      flex: 1,
      backgroundColor: '#fff',
      margin: 50,
      alignItems: 'center',
      justifyContent: 'center',
    },
    TextStyle: {
      fontSize: 23,
      textAlign: 'center',
      color: '#f00',
    },
  });