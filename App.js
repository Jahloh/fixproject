/*Example of SQLite Database in React Native*/
import React from 'react';
//For react-navigation 3.0+
//import { createAppContainer, createStackNavigator } from 'react-navigation';
//For react-navigation 4.0+
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import FirstPage from './pages/FirstPage';
import SecondPage from './pages/SecondPage';
import Dash from './pages/Dash.js';
import HomeScreen from './pages/HomeScreen';
import RegisterUser from './pages/RegisterUser';
import UpdateUser from './pages/UpdateUser';
import ViewUser from './pages/ViewUser';
import ViewAllUser from './pages/ViewAllUser';
import DeleteUser from './pages/DeleteUser';
import SelectPage from './pages/SelectPage';
import SelectPagesecond from './pages/SelectPagesecond';
import Condem from './pages/components/Condem';
import Trial from './pages/components/Trial';
import Convict from './pages/components/Convict';
import LifeInprisonments from './pages/components/LifeInprisonments'
import Mental from './pages/components/Mental';
import MyModals from './pages/components/MyModals';
import SimpleModal from './pages/components/SimpleModal';
 
const App = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Fix Prisons Counter App',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  SelectPage: {
    screen: SelectPage,
    navigationOptions: {
      title: 'SelectPage',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  MyModals: {
    screen: MyModals,
    navigationOptions: {
      title: 'MyModals',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  SimpleModal: {
    screen: SimpleModal,
    navigationOptions: {
      title: 'SimpleModal',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Dash: {
    screen: Dash,
    navigationOptions: {
      title: 'Dash',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Condemns: {
    screen: Condem,
    navigationOptions: {
      title: 'Condemn Dashboard',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Trialss: {
    screen: Trial,
    navigationOptions: {
      title: 'Trials Dashboard',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Convictss: {
    screen: Convict,
    navigationOptions: {
      title: 'Convict Dashboard',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  ForLife: {
    screen: LifeInprisonments,
    navigationOptions: {
      title: 'Life Inprisonments Dashboard',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  ForMentalCases: {
    screen: Mental,
    navigationOptions: {
      title: 'Mental Dashboard',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
    SelectPagesecond: {
      screen: SelectPagesecond,
      navigationOptions: {
        title: 'SelectPagesecond',
        headerStyle: { backgroundColor: '#004D40' },
        headerTintColor: '#ffffff',
      },
    },
      
  View: {
    screen: ViewUser,
    navigationOptions: {
      title: 'View Facilities',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  ViewAll: {
    screen: ViewAllUser,
    navigationOptions: {
      title: 'View All Facilities',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Update: {
    screen: UpdateUser,
    navigationOptions: {
      title: 'Transfer Inmates',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Register: {
    screen: RegisterUser,
    navigationOptions: {
      title: 'Remand Dashboard',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  Delete: {
    screen: DeleteUser,
    navigationOptions: {
      title: 'Release Inmates',
      headerStyle: { backgroundColor: '#004D40' },
      headerTintColor: '#ffffff',
    },
  },
  //Constant which holds all the screens like index of any book 
  FirstPage: { screen: FirstPage }, 
  //First entry by default be our first screen 
  //if we do not define initialRouteName
  SecondPage: { screen: SecondPage }, 

// ,{
//   initialRouteName: 'FirstPage',
//
 });
export default createAppContainer(App);