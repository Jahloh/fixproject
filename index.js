/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
//import ViewAllUser from './pages/ViewAllUser';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
